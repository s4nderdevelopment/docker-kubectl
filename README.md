# kubectl Docker image

Repository containing a dockerfile for building an image containing kubectl and bash for GitLab CI.

Docker Hub: [https://hub.docker.com/r/s4nder/kubectl-with-bash](https://hub.docker.com/r/s4nder/kubectl-with-bash)
